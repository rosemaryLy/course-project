## course project

This project covers the following topics:
- Node.js configuration and setup
- API servers
- Express routes
- HTTP methods
- Middleware
- Token-based-authentification (JWT) and protected routes
- Password hashing
- Creating json files and writing to them

## Installation

1. Run `npm install` to install dependencies.
2. Run`database/schema.sql` and `database/seeds.sql` against database.
3. Run `npm run dev` to start server. 
