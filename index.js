import express from 'express';
import router from './src/router';
import {checkFile, createTestUser} from './src/util';
import cors from 'cors'
import bodyParser from 'body-parser'


const app = express();
const port = 4000;

app.use(cors());
app.use(express.json());
app.use(router);
app.use(bodyParser.json());

checkFile("entries.json");
checkFile("users.json", createTestUser);

app.listen(port, () => console.log(`API server ready on http://localhost:${port}`))

