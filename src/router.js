import express from "express";
import contactForm from "./routers/contact";
import users from "./routers/users";
import auth from "./routers/auth";
import resume from "./routers/resume";
import portfolio from "./routers/portfolio";

const router = express.Router();


router.use("/contact_form", contactForm);
router.use("/users", users);
router.use("/auth", auth);
router.use("/resume", resume);
router.use("/portfolio", portfolio);

router.get("*", (req, res) => {
    return res.status(404).json({message: "not found"})
  });

export default router